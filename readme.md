# HTTP Redirection Service

This project is a fork of [Suckless' Quark][1] project. Go check them out, as they really did most of the work here.

## Building

Building the project is fairly simple:

1. Change `config.h` to your liking
2. Change `config.mk` to match your system
3. run `make install`

## Usage

This tool works in the same way as any other web server, so it expects to be given a directory of files. Because this is only a redirection service, it expects the files to have the URL to redirect to as the first (and only) line in each file. This means you can manage your redirections easily with the shell, or the programming language of your choice.

As an example, lets assume you had the service running on `https://re.org`, and it was looking in the folder `/srv/re`. To make `https://re.org/ex` redirect to `https://example.com`, you can simply write:

```
echo 'https://example.com' > /srv/re/ex
```

and your done.



[1]: https://tools.suckless.org/quark/
