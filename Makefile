# See LICENSE file for copyright and license details
# re - simple redirection server
.POSIX:

include config.mk

COMPONENTS = util sock http resp

all: re

util.o: util.c util.h config.mk
sock.o: sock.c sock.h util.h config.mk
http.o: http.c http.h util.h http.h resp.h config.h config.mk
resp.o: resp.c resp.h util.h http.h config.mk
main.o: main.c util.h sock.h http.h arg.h config.h config.mk

re: $(COMPONENTS:=.o) $(COMPONENTS:=.h) main.o config.mk
	$(CC) -o $@ $(CPPFLAGS) $(CFLAGS) $(COMPONENTS:=.o) main.o $(LDFLAGS)

config.h:
	cp config.def.h $@

clean:
	rm -f re main.o $(COMPONENTS:=.o)

dist:
	rm -rf "re-$(VERSION)"
	mkdir -p "re-$(VERSION)"
	cp -R LICENSE Makefile arg.h config.def.h config.mk re.1 \
		$(COMPONENTS:=.c) $(COMPONENTS:=.h) main.c "re-$(VERSION)"
	tar -cf - "re-$(VERSION)" | gzip -c > "re-$(VERSION).tar.gz"
	rm -rf "re-$(VERSION)"

install: all
	mkdir -p "$(DESTDIR)$(PREFIX)/bin"
	cp -f re "$(DESTDIR)$(PREFIX)/bin"
	chmod 755 "$(DESTDIR)$(PREFIX)/bin/re"
	mkdir -p "$(DESTDIR)$(MANPREFIX)/man1"
	cp re.1 "$(DESTDIR)$(MANPREFIX)/man1/re.1"
	chmod 644 "$(DESTDIR)$(MANPREFIX)/man1/re.1"

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/re"
	rm -f "$(DESTDIR)$(MANPREFIX)/man1/re.1"
